package org.tio.websocket.starter;

import org.tio.core.intf.GroupListener;

/**
 * @author fanpan26
 * */
public interface TioWebSocketGroupListener extends GroupListener {
}
