package org.tio.websocket.starter;

import org.tio.core.stat.IpStatListener;

/**
 * @author fanpan26
 * */
public interface TioWebSocketIpStatListener extends IpStatListener {
}
